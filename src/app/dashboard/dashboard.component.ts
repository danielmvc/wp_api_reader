import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from '../../../node_modules/rxjs';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  router: Router;
  apiUrl = 'http://marketing.myvirtualclinic.ca/wp-json/wp/v2/';
  mediaUrl = 'media';
  categoriesUrl = 'categories';
  postsUrl = 'posts';
  counter$;
  postsPerCategory: {};
  categories: {};
  media: {};
  AllCategories = [];

  constructor(private http: HttpClient) {
    this.counter$ = Observable.interval(1000);
  }

  ngOnInit() {
    this.counter$.subscribe(() => {
      this.http.get(this.apiUrl + this.mediaUrl)
        .subscribe(response => this.media = response);
      this.http.get(this.apiUrl + this.categoriesUrl)
        .subscribe(response => {
          this.categories = response;
          for (let i = 0; i < Object.keys(this.categories).length; i++) {
            if (this.categories[i].count > 0) {
              this.http.get(this.apiUrl + this.postsUrl + '?' + this.categoriesUrl + '=' + this.categories[i].id)
                .subscribe(res => {
                  this.postsPerCategory = res;
                  this.AllCategories[this.categories[i].id] = this.postsPerCategory;
                });
            }
          }
        });
    });
  }
}
