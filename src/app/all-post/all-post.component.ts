import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Observable} from '../../../node_modules/rxjs';

@Component({
  selector: 'app-all-post',
  templateUrl: './all-post.component.html',
  styleUrls: ['./all-post.component.scss']
})
export class AllPostComponent implements OnInit {
  apiUrl = 'http://marketing.myvirtualclinic.ca/wp-json/wp/v2/';
  categoriesUrl = 'categories';
  mediaUrl = 'media';
  categoryIdUrl;
  media: {};
  categories: {};
  posts: {};
  counter$;

  constructor(private route: ActivatedRoute, private http: HttpClient) {
    this.counter$ = Observable.interval(1000);
  }

  ngOnInit() {

    // this.counter$.subscribe(() => {
    this.categoryIdUrl = this.route.snapshot.params['id'];
    this.http.get(this.apiUrl + this.mediaUrl)
      .subscribe(response => this.media = response);
    this.http.get(this.apiUrl + this.categoriesUrl)
      .subscribe(response => this.categories = response);
    this.http.get(this.apiUrl + 'posts?categories=' + this.categoryIdUrl)
      .subscribe(response => this.posts = response);
    console.log(this.posts);
    // });
  }

}
