import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Observable} from '../../../node_modules/rxjs';

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.scss']
})
export class SinglePostComponent implements OnInit {
  apiUrl = 'http://marketing.myvirtualclinic.ca/wp-json/wp/v2/';
  postsUrl = 'posts/';
  postIdUrl;
  categoriesUrl = 'categories';
  authorUrl = 'users';
  mediaUrl = 'media';
  counter$;
  post = {};
  media: {};
  authors: {};
  categories: {};
  date: Date;

  constructor(private route: ActivatedRoute, private http: HttpClient) {
    this.counter$ = Observable.interval(1000);
    this.postIdUrl = this.route.snapshot.params['id'];
  }

  ngOnInit() {
    this.postIdUrl = this.route.snapshot.params['id'];
    this.http.get(this.apiUrl + this.postsUrl + this.postIdUrl)
      .subscribe(response => this.post = response);
    this.http.get(this.apiUrl + this.mediaUrl)
      .subscribe(response => this.media = response);
    this.http.get(this.apiUrl + this.authorUrl)
      .subscribe(response => this.authors = response);
    this.http.get(this.apiUrl + this.categoriesUrl)
      .subscribe(response => this.categories = response);
  }
}
